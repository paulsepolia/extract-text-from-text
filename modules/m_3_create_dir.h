//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/08/15              //
//===============================//

#include <iostream>
#include <string>
#include <sys/stat.h>
#include <errno.h>

using std::endl;
using std::string;
using std::cout;

int mkpath(string s, mode_t mode)
{
  //  1. Local variables
  
  size_t pre;
  size_t pos;
  string dir;
  int mdret;

  //  2. Initialize local variables

  pre = 0;

  //  3. Append '/'

  if(s[s.size()-1] != '/')
  {
    // Force trailing. So we can handle everything in loop
    s += '/';
  }

  //  4. Create the directory

  while((pos=s.find_first_of('/', pre)) != string::npos)
  {
    dir = s.substr(0, pos++);
    pre = pos;
    if(dir.size() == 0)
    { continue; } // if leading. First time is 0 length
      
    if((mdret = mkdir(dir.c_str(), mode)) && (errno != EEXIST))
    { return mdret; }
  }
 
  //  5. Exit
 
  return mdret;
}

//======//
// FINI //
//======//
