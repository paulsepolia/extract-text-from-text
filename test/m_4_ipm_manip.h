//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/08/15              //
//===============================//

#include <iostream>
#include <fstream>
#include <string>
#include <array>
#include <ctime>
#include <cstdlib>
#include <sstream>

#include "m_3_create_dir.h"

using std::ifstream;
using std::ofstream;
using std::endl;
using std::cout;
using std::ios;
using std::string;
using std::array;
using std::stringstream;

int ipm_manip( string &user_output_old, \
               string &ipm_output,      \
               string &user_output_new, \
               string &head_output_dir, \
               int &debug_flag )
{
  //  1. Local variables

  static int i_pass = 0;       // times the function has been called
  ifstream file_stream;        // file stream to read only the user's output
  ofstream ipm_file_stream;    // file stream to write the ipm info
  ofstream out_file_stream;    // file stream to write the new user's output
  char* file_buffer = 0;       // a character container
  long i;                      // iterator
  long file_length;            // length of the file stream
  long i_tmp;                  // help variable
  long i_loc_a;                // counter. Number of ipm starting points
  long i_loc_b;                // counter. Number of ipm middle points
  long i_loc_c;                // counter. Number of ipm end points
  array<long,MAX_LEN> array_a; // array to hold the position of the begin of ipm output
  array<long,MAX_LEN> array_b; // array to hold the middle verification position
  array<long,MAX_LEN> array_c; // array to hold the position of the end of ipm output
  string s_tmp_a;              // help string
  static string s_tmp_b;       // help string
  static string s_tmp_c;       // help string
  stringstream ss_tmp;         // help stringstream

  //  2-1. Open the file stream to read users old output file

  file_stream.open(user_output_old);

  //  2-2. Test if opened or not

  if(!file_stream.is_open())
  { if (debug_flag == 1)
    { cout << " 1 --> Error. User's ascii file can not be opened." << endl;
      cout << " 2 --> Error. Stop." << endl; }
    return ERROR_FLAG; }
  else if(file_stream.is_open())
  { if (debug_flag == 1) 
    { cout << " 3 --> User's ascii file: " << user_output_old << ", opened." << endl; }}

  //  3. Get the length of user's file

  file_stream.seekg(0, ios::end);
  file_length = file_stream.tellg();
  file_stream.seekg(0, ios::beg);

  //  4. Allocate RAM for "file_buffer" and
  //     read data as a block to buffer object

  file_buffer = new char [file_length];
  file_stream.read(file_buffer, file_length);

  //  5. Close "file_stream" stream

  file_stream.close();

  //  6-1. Look for the pattern: ##IPMv0.983#pgg_ipm_1##
  //       and get back the record when the pattern starts.
  //       The above pattern marks the beginning of the IPM appended info

  //  6-2. Reset the counter

  i_loc_a = 0;

  //  6-3. Search for the pattern
 
  for(i = 0; i < file_length; i++)
  {
    if(
        file_buffer[i+22] == '#' && \
        file_buffer[i+21] == '#' && \
        file_buffer[i+20] == '1' && \
        file_buffer[i+19] == '_' && \
        file_buffer[i+18] == 'm' && \
        file_buffer[i+17] == 'p' && \
        file_buffer[i+16] == 'i' && \
        file_buffer[i+15] == '_' && \
        file_buffer[i+14] == 'g' && \
        file_buffer[i+13] == 'g' && \
        file_buffer[i+12] == 'p' && \
        file_buffer[i+11] == '#' && \
        file_buffer[i+10] == '3' && \
        file_buffer[i+9]  == '8' && \
        file_buffer[i+8]  == '9' && \
        file_buffer[i+7]  == '.' && \
        file_buffer[i+6]  == '0' && \
        file_buffer[i+5]  == 'v' && \
        file_buffer[i+4]  == 'M' && \
        file_buffer[i+3]  == 'P' && \
        file_buffer[i+2]  == 'I' && \
        file_buffer[i+1]  == '#' && \
        file_buffer[i+0]  == '#'
      )
      { array_a[i_loc_a] = i;
        i_loc_a = i_loc_a+1; }
  }

  //  7-1. Look for the pattern: ##IPMv0.983#pgg_ipm_2##
  //       and get back the record the pattern starts.
  //       The above pattern marks the beggining of the IPM appended info
 
  //  7-2. Reset the counter

  i_loc_b = 0;

  //  7-3. Search for the pattern

  for(i = 0; i < file_length ; i++)
  {
    if(
        file_buffer[i+22] == '#' && \
        file_buffer[i+21] == '#' && \
        file_buffer[i+20] == '2' && \
        file_buffer[i+19] == '_' && \
        file_buffer[i+18] == 'm' && \
        file_buffer[i+17] == 'p' && \
        file_buffer[i+16] == 'i' && \
        file_buffer[i+15] == '_' && \
        file_buffer[i+14] == 'g' && \
        file_buffer[i+13] == 'g' && \
        file_buffer[i+12] == 'p' && \
        file_buffer[i+11] == '#' && \
        file_buffer[i+10] == '3' && \
        file_buffer[i+9]  == '8' && \
        file_buffer[i+8]  == '9' && \
        file_buffer[i+7]  == '.' && \
        file_buffer[i+6]  == '0' && \
        file_buffer[i+5]  == 'v' && \
        file_buffer[i+4]  == 'M' && \
        file_buffer[i+3]  == 'P' && \
        file_buffer[i+2]  == 'I' && \
        file_buffer[i+1]  == '#' && \
        file_buffer[i+0]  == '#'
      )
      { array_b[i_loc_b] = i;
        i_loc_b = i_loc_b + 1; }
  }

  //  8-1. Look for the pattern: # #IPMv0.983#pgg_ipm_3# # (pattern is broken on purpose)
  //       and get back the record the pattern starts.
  //       The above pattern marks the beggining of the IPM appended info

  //  8-2. Reset the counter

  i_loc_c = 0;

  //  8-3. Search for the pattern
 
  for(i = 0; i < file_length; i++)
  {
    if(
        file_buffer[i+22] == '#' && \
        file_buffer[i+21] == '#' && \
        file_buffer[i+20] == '3' && \
        file_buffer[i+19] == '_' && \
        file_buffer[i+18] == 'm' && \
        file_buffer[i+17] == 'p' && \
        file_buffer[i+16] == 'i' && \
        file_buffer[i+15] == '_' && \
        file_buffer[i+14] == 'g' && \
        file_buffer[i+13] == 'g' && \
        file_buffer[i+12] == 'p' && \
        file_buffer[i+11] == '#' && \
        file_buffer[i+10] == '3' && \
        file_buffer[i+9]  == '8' && \
        file_buffer[i+8]  == '9' && \
        file_buffer[i+7]  == '.' && \
        file_buffer[i+6]  == '0' && \
        file_buffer[i+5]  == 'v' && \
        file_buffer[i+4]  == 'M' && \
        file_buffer[i+3]  == 'P' && \
        file_buffer[i+2]  == 'I' && \
        file_buffer[i+1]  == '#' && \
        file_buffer[i+0]  == '#'
      )
      { array_c[i_loc_c] = i;
        i_loc_c = i_loc_c + 1; }
  }

  //  9. Test for success.
  //     If the test is okay then continue,
  //     else returns.

  if ((i_loc_a == i_loc_b) && (i_loc_b == i_loc_c) && (i_loc_a != 0)) 
  { if (debug_flag == 1) {
    cout << " 4 --> Success. The ipm produced ascii output has been located." << endl; }}
  else if ( (i_loc_a != i_loc_b) || \
            (i_loc_b != i_loc_c) || \
            (i_loc_a == 0)       || \
            (i_loc_b == 0)       || \
            (i_loc_c == 0) )
  { if (debug_flag == 1) {
    cout << " 5 --> Return 0. The ipm produced ascii output has not been located." << endl; }
    return 0; }

  // 10-1. Open "ipm_file_stream" stream

  ipm_file_stream.open(ipm_output, ios::out);

  // 10-2. Test if opened or not

  if(!ipm_file_stream.is_open())
  { if (debug_flag == 1) 
    { cout << " 6 --> Error. ipm new ascii file can not be opened." << endl;
    cout << " Error. Stop." << endl; }
    return ERROR_FLAG; }
  else if(ipm_file_stream.is_open())
  { if (debug_flag == 1) 
    { cout << " 7 --> ipm new ascii file: " << ipm_output << ", opened." << endl; }}

  // 11. Write to the "ipm_file_stream" stream

  for (i = 0; i < i_loc_a; i++)
  {
    ipm_file_stream.write(file_buffer+array_a[i], array_c[i]-array_a[i]+OFF_SET);
  }

  // 12. Close "ipm_file_stream"
  
  ipm_file_stream.close();

  // 13-1. Open "out_file_stream" stream

  out_file_stream.open(user_output_new, ios::out);

  // 13-2. Test if opened or not

  if(!out_file_stream.is_open())
  { if (debug_flag == 1) { cout << " 8 --> Error. Output new ascii file." << endl;
    cout << " 9 --> Error. Stop." << endl; }
    return ERROR_FLAG; }
  else if(out_file_stream.is_open())
  { if (debug_flag == 1) 
    { cout << " 10 --> Output new ascii file: " << user_output_new
           << ", opened." << endl; }}

  // 14-1. Write to the "out_file_stream" stream

  for (i = 0; i <= i_loc_a; i++)
  {
    if (i == 0)
    { out_file_stream.write(file_buffer,array_a[i]); }
    else if ((i < i_loc_a) && (i != 0))
    { out_file_stream.write(file_buffer+array_c[i-1]+OFF_SET, array_a[i]-array_c[i-1]-OFF_SET); }
    else if (i == i_loc_a)
    { out_file_stream.write(file_buffer+array_c[i-1]+OFF_SET, file_length-array_c[i-1]-OFF_SET); }
  }

  // 14-2. Close the "out_file_stream" stream

  out_file_stream.close();

  // 15-1. Remove users's output with ipm content

  const char *ch1 = user_output_old.c_str();
  remove(ch1);

  // 15-2. Restore user's output names

  const char *ch2 = user_output_new.c_str();
  rename(ch2, ch1);

  // 15-3. Free RAM

  delete[] file_buffer;

  // 16. Create the directory

  if (i_pass == 0)
  {
    i_tmp = time(NULL);
    ss_tmp << i_tmp;
    ss_tmp << ".";
    ss_tmp << getpid();
    ss_tmp >> s_tmp_a;

    s_tmp_b = head_output_dir + s_tmp_a;
 
    i_tmp = mkpath(s_tmp_b, 0777);

    if (i_tmp != 0) 
    { if (debug_flag == 1)
      { cout << "Error. Can not create the output directory." << endl; 
        cout << "Exit."; }
      exit(ERROR_FLAG);
    }
  }

  // 17. Increase the 'i_pass' counter

  i_pass = i_pass + 1;

  // 18-1. Copy the ipm file

  // 18-2. Open the stream

  file_stream.open(ipm_output, ios::in);

  // 18-3. Test if opened with success

  if(!file_stream.is_open())
  { if (debug_flag == 1)
    { cout << " 11 --> Error. ipm ascii file can not be opened." << endl;
      cout << " 12 --> Error. Stop." << endl; }
    return ERROR_FLAG; }
  else if(file_stream.is_open())
  { if (debug_flag == 1) 
    { cout << " 13 --> ipm ascii file: " << ipm_output << ", opened." << endl; }}

  // 18-4. Get length of ipm file

  file_stream.seekg(0, ios::end);
  file_length = file_stream.tellg();
  file_stream.seekg(0, ios::beg);

  // 18-5. Read ipm file

  file_buffer = new char [file_length];
  file_stream.read(file_buffer, file_length);

  // 18-6. Set new path and name for the ipm file

  s_tmp_c = s_tmp_b + "/" + ipm_output;

  // 18-7. Open the stream to copy the ipm file in the new location

  out_file_stream.open(s_tmp_c, ios::out);

  // 18-8. Test if opened with success

  if(!out_file_stream.is_open())
  { if (debug_flag == 1)
    { cout << " 14 --> Error. ipm new ascii file can not be opened." << endl;
      cout << " 15 --> Error. Stop." << endl; }
    return ERROR_FLAG; }
  else if(out_file_stream.is_open())
  { if (debug_flag == 1) 
    { cout << " 16 --> ipm new ascii file: " << ipm_output << ", opened." << endl; }}

  // 18-9. Write the ipm into new location

  out_file_stream.write(file_buffer, file_length);
  
  // 18-10. Close the streams and free RAM

  out_file_stream.close();
  file_stream.close();

  delete [] file_buffer;

  // 19. Remove ipm file from the working directory

  const char *ch3 = ipm_output.c_str();
  remove(ch3);

  // 20. Exit

  return 0;
}

//======//
// FINI //
//======//
