#!/bin/bash

  # 1. compile

  icpc -O3                 \
       -w3                 \
       -Wall               \
       -static-intel       \
       -static             \
       -ansi               \
       -std=c++0x          \
       -Weffc++            \
       driver_ipm_hpcw.cpp \
       -o x_ipm

  # 2. exit
