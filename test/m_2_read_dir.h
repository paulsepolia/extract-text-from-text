//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/08/15              //
//===============================//

#include <dirent.h>
#include <iostream>
#include <cstdlib>
#include <array>
#include <cstring>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <fstream>

using std::cout;
using std::cin;
using std::endl;
using std::array;
using std::string;
using std::ifstream;

int read_dir( array<string,MAX_LEN> &files_array, \
              int &files_num,                     \
              int &debug_flag )
{
  //  1. Local variables

  int i;
  int i_counter;
  DIR *pdir = NULL;
  struct dirent *pent = NULL;
  struct stat _buf;
  int ch_tmp;

  //  2. Open the current directory

  pdir = opendir(".");

  //  3. Test if 'pdir' is initialized correctly
 
  if (pdir == NULL)
  { 
    if (debug_flag == 1)
    { cout << " 1 --> Error. 'pdir' could not be initialised correctly." << endl; } 
    return ERROR_FLAG;
  }
  else
  {
    if (debug_flag == 1)
    { cout << " 2 --> Success. 'pdir' initialized correctly." << endl; }
  }

  //  4-1. Get number of files in the directory

  //  4-2. Reset the file counter

  i_counter = 0;

  //  4-3. Get files number

  while(pent = readdir(pdir))
  {
    //  a. Test for end of directory

    if (pent == NULL)
    { 
      if (debug_flag == 1)
      { cout << " 3 --> Error. 'pent' could not be initialised correctly" << endl; }  
      return ERROR_FLAG; 
    }
    else
    {
      if (debug_flag == 1)
      { cout << " 4 --> Success. The 'pent' initialized correctly." << endl; }
    }


    //  b. test if

    if (i_counter > MAX_LEN)
    { 
      if (debug_flag == 1)
      { cout << " 3 --> Error. Too many files." << endl; }
      return ERROR_FLAG;
    }

    //  c. Skip ".", ".." and any subdirectory

    if (
        (strcmp(pent -> d_name, ".")  != 0)  && \
        (strcmp(pent -> d_name, "..") != 0)  && \
        !((stat(pent -> d_name, &_buf) == 0) && \
          (S_ISDIR(_buf.st_mode)))
       )
    { // check if file is binary, if not add it to the list
      ifstream ifs_tmp(pent->d_name);
      while(((ch_tmp = ifs_tmp.get()) != EOF) && (ch_tmp <= 127));
      if (ch_tmp == EOF) 
      {  
        files_array[i_counter] = (pent -> d_name);
        i_counter++;
      }
    }
  }

  //  5. Set 'files_num'

  files_num = i_counter;

  //  6. Close the directory

  closedir(pdir);

  //  7. Exit with success 
 
  return 0;
}

//======//
// FINI //
//======//
